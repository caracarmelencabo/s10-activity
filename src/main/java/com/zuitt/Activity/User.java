package com.zuitt.Activity;

public class User {
    // Properties
    private String name;

    // Constructors
    public User(){}
    public User(String name, String address){
        this.name = name;
    }
    // Getters
    public String getName(){
        return name;
    }
}
