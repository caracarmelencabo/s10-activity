package com.zuitt.Activity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class ActivityApplication {

	public static void main(String[] args) {
		SpringApplication.run(ActivityApplication.class, args);
	}

	// Get all users
	@GetMapping("/users")
	public String getUsers(){
		return "All users retrieved";
	}

	// Create User
	@PostMapping("/users")
	public String createUser(){
		return "New user created";
	}

	// Get a specific user
	@GetMapping("/users/{userid}")
	public String getSpecificUser(@PathVariable Long userid){
		return "Viewing details of user " + userid;
	}

	// Delete a user
	@DeleteMapping("/users/{userid}")
	public String deleteUser(@PathVariable Long userid, @RequestHeader("Authorization") String authorization){
		if (authorization == null || authorization.isEmpty()) {
			return "Unauthorized access";
		}else{
			return "The user " + userid + " has been deleted";
		}
	}

	// Update user
	@PutMapping("/users/{userid}")
	@ResponseBody
	public User updateUser(@PathVariable Long userid, @RequestBody User user){
		return user;
	}

}
